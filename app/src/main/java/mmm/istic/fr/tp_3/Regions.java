package mmm.istic.fr.tp_3;

public final class Regions {
    /**
     * Our data, part 1.
     */
    public static final String[] TITLES =
            {
                    "Alsace",
                    "Beaujolais",
                    "Jura",
                    "Champagne",
                    "Savoie",
                    "Languedoc",
                    "Roussillon",
                    "Bordelais",
                    "Vallée du Rhone",
                    "Val de Loire",
                    "Sud-Ouest",
                    "Bourgogne",
                    "Corse"
            };

    /**
     * Our data, part 2.
     */

    static String[] DIALOGUE = {

            "http://technoresto.org/vdf/alsace/index.html",
            "http://technoresto.org/vdf/beaujolais/index.html",
            "http://technoresto.org/vdf/jura/index.html",
            "http://technoresto.org/vdf/champagne/index.html",
            "http://technoresto.org/vdf/savoie/index.html",
            "http://technoresto.org/vdf/languedoc/index.html",
            "http://technoresto.org/vdf/bordelais/index.html",
            "http://technoresto.org/vdf/cotes_du_rhone/index.html",
            "http://technoresto.org/vdf/provence/index.html",
            "http://technoresto.org/vdf/val_de_loire/index.html",
            "http://technoresto.org/vdf/sud-ouest/index.html",
            "http://technoresto.org/vdf/bourgogne/index.html",
            "http://technoresto.org/vdf/corse/index.html"

    };

//    static LatLng[] LOCATION = {
//            new LatLng(48.424375, 7.468201),
//            new LatLng(46.136559, 4.722682),
//            new LatLng(46.7828854, 5.1674246),
//            new LatLng(48.0159215, 0.3012291),
//            new LatLng(45.4946988, 5.8419023),
//            new LatLng(44.7246043, 3.852038),
//            new LatLng(44.837553, -0.581056),
//            new LatLng(44.049515, 5.079814),
//            new LatLng(44.0509021, 4.8506238),
//            new LatLng(47.6256951, -0.6582459),
//            new LatLng(45.500438, -0.720430),
//            new LatLng(42.1771279, 7.9259932),
//            new LatLng(47.2597945, 1.9329833)
//    };

}
